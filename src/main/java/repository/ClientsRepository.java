package repository;

import models.Clients;
import models.Practician;
import utility.ClientsParser;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.Iterator;
import java.util.List;

public class ClientsRepository {
    private EntityManager entityManager;

    public ClientsRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    public Clients saveClients(Clients clients) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            entityManager.persist(clients);
            entityTransaction.commit();
            return clients;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Clients> findAll() {
        return entityManager.createQuery("FROM clients", Clients.class).getResultList();
    }

    public List<Clients> findByNameAndrei() {
        return entityManager.createQuery("FROM clients WHERE name='Andrei'", Clients.class).getResultList();

    }
//    Query query = session.createQuery("from Employee e where e.idEmployee=:id");
//query.setParameter("id", id);

    public List<Clients> findByName(String name) {
        Query query =entityManager.createQuery("FROM clients WHERE name=:name");
        query.setParameter("name",name);
        return query.getResultList();
//        return entityManager.createQuery("FROM clients WHERE name='Andrei'", Clients.class).getResultList();


    }
    public Clients findById(Integer id) {
        return entityManager.find(Clients.class, id);
    }

//    public List<Clients> findByName(String name){
//        return entityManager.find(Clients.class,name);
//    }

    public Clients deleteClients(Clients clients) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            entityManager.remove(clients);
            entityTransaction.commit();
            return clients;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Clients> saveAllClients(List<Clients> clientsList){
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            for (Clients clients : clientsList) {
                entityManager.persist(clients);

            }
            entityTransaction.commit();
            return clientsList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Clients> deleteAllClients(List<Clients> clientsList){
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            for (Clients clients : clientsList) {
                entityManager.remove(clients);

            }
            entityTransaction.commit();
            return clientsList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
