package repository;

import models.Schedule;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Iterator;
import java.util.List;

public class ScheduleRepository {

    private EntityManager entityManager;

    public ScheduleRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    public Schedule saveSchedule(Schedule schedule) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            entityManager.persist(schedule);
            entityTransaction.commit();
            return schedule;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Schedule> findAll() {
        return entityManager.createQuery("FROM schedule", Schedule.class).getResultList();
    }

    public Schedule findById(Integer id) {  // metodă READ_One
        return entityManager.find(Schedule.class, id);
    }

    public Schedule deleteSchedule(Schedule schedule) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            entityManager.remove(schedule);
            entityTransaction.commit();
            return schedule;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Schedule> saveAllSchedules(List<Schedule> scheduleList){
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            for (Schedule schedule : scheduleList) {
                entityManager.persist(schedule);

            }
            entityTransaction.commit();
            return scheduleList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Schedule> deleteAllSchedules(List<Schedule> scheduleList){
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            for (Schedule schedule : scheduleList) {
                entityManager.remove(schedule);

            }
            entityTransaction.commit();
            return scheduleList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
