package repository;

import models.Practician;
import models.Schedule;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Iterator;
import java.util.List;

public class PracticianRepository {
    private EntityManager entityManager;

    public PracticianRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    public Practician savePractician(Practician practician) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            entityManager.persist(practician);
            entityTransaction.commit();
            return practician;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Practician> findAll() {
        return entityManager.createQuery("FROM practician", Practician.class).getResultList();
    }

    public Practician findById(Integer id) {
        return entityManager.find(Practician.class, id);
    }

    public Practician deletePractician(Practician practician) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            entityManager.remove(practician);
            entityTransaction.commit();
            return practician;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Practician> saveAllPractician(List<Practician> practicianList){
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            for (Practician practician : practicianList) {
                entityManager.persist(practician);

            }
            entityTransaction.commit();
            return practicianList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Practician> deleteAllPractician(List<Practician> practicianList){
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            for (Practician practician : practicianList) {
                entityManager.remove(practician);

            }
            entityTransaction.commit();
            return practicianList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
