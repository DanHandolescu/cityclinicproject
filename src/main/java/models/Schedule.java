package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "schedule")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Schedule {
    @Id
    @GeneratedValue
    @Column(
            name = "schedule_id"
    )
    private int id;
    @Column(
            name = "schedule_date"
    )
    private LocalDateTime scheduleDate;
    @JoinColumn(name = "clients_id",
            referencedColumnName = "clients_id")
    @ManyToOne
    private Clients clients;
    @JoinColumn(name = "practician_id",
            referencedColumnName = "practician_id")
    @ManyToOne
    private Practician practician;

    public Schedule(LocalDateTime scheduleDate, Clients clients, Practician practician) {
        this.scheduleDate = scheduleDate;
        this.clients = clients;
        this.practician = practician;
    }
}