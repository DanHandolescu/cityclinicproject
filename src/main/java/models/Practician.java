package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity(name = "practician")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Practician {
    @Id
    @GeneratedValue
    @Column(name = "practician_id")
    private int id;
    @Column(name = "practician_speciality")
    private String speciality;
    @Column(name = "practician_name")
    private String name;
    @OneToMany(mappedBy = "practician")
    private List<Schedule> schedules;

    public Practician(int id, String speciality, String name) {
        this.id = id;
        this.speciality = speciality;
        this.name = name;
    }
    public Practician(String speciality, String name) {
        this.speciality = speciality;
        this.name = name;
    }
}