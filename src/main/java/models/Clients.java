package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity(name = "clients")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Clients {
    @Id
    @GeneratedValue
    @Column(
        name = "clients_id"
    )
    private int id;
    @Column(
        name = "clients_name"
    )
    private String name;
    @Column(
        name = "clients_address"
    )
    private String address;
    @Column(
        name = "clients_phone_nr"
    )
    private String phoneNr;
    @Column(
        name = "clients_birthday"
    )
    private LocalDate birthday;
    @OneToMany(
        mappedBy = "clients"
    )
    private List<Schedule> schedules;

    public Clients(String name, String address, String phoneNr, LocalDate birthday) {
        this.name = name;
        this.address = address;
        this.phoneNr = phoneNr;
        this.birthday = birthday;
    }
}