package utility;

import models.Clients;
import org.hibernate.SessionFactory;
import repository.ClientsRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Main4 {
    public static void main(String[] args) {

    Path filePath = Paths.get("export5.csv");
    final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    final ClientsRepository clientsRepository = new ClientsRepository(sessionFactory.createEntityManager());

    List<Clients> clients = clientsRepository.findByName("Dan");
        for (Clients client : clients) {
        String c = ClientsParser.toCSV(client);
        try {
            Files.writeString(filePath, c,StandardOpenOption.APPEND);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

//        Clients clients = clientsRepository.findByName("Ion");
//        for (Clients client : clients) {
//        String c = ClientsParser.toCSV(client);
//        try {
//            Files.writeString(filePath, c,StandardOpenOption.APPEND);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }

}}
