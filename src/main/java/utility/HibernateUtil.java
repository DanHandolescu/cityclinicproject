package utility;

import models.Clients;
import models.Practician;
import models.Schedule;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    public HibernateUtil() {
    }

    public static SessionFactory getSessionFactory() {
        SessionFactory sessionFactory = (new Configuration()).configure("hibernate.cfg.xml").addAnnotatedClass(Clients.class).addAnnotatedClass(Practician.class).addAnnotatedClass(Schedule.class).buildSessionFactory();
        return sessionFactory;
    }
}
