package utility;

import models.Clients;
import models.Practician;
import models.Schedule;
import org.hibernate.SessionFactory;
import repository.ClientsRepository;
import repository.PracticianRepository;
import repository.ScheduleRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class Main3 {
    public static void main(String[] args) {
        Practician practician1=new Practician("Cardiology","Vasile");
        Clients client1= new Clients("Dorel","Bucuresti","075643235", LocalDate.of(1945,12,10));
        Schedule schedule1=new Schedule( LocalDateTime.of(2022,12,01,12,00),client1,practician1);

//        Schedule schedule2=new Schedule( LocalDateTime.of(2022,12,01,13,00),new Clients("Ioana","Cluj","07565233235", LocalDate.of(1998,10,15)),practician1);
//
//        Schedule schedule3=new Schedule( LocalDateTime.of(2022,12,01,14,00),new Clients("Ion","Constanta","07747758", LocalDate.of(1959,05,19)),practician1);


        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        PracticianRepository practicianRepository=new PracticianRepository(sessionFactory.createEntityManager());
        practicianRepository.savePractician(practician1);
        ClientsRepository clientsRepository=new ClientsRepository(sessionFactory.createEntityManager());
        clientsRepository.saveClients(client1);
        ScheduleRepository scheduleRepository=new ScheduleRepository(sessionFactory.createEntityManager());


        List<Schedule> myListOfSchedules= Arrays.asList(schedule1);
        scheduleRepository.saveAllSchedules(myListOfSchedules);

    }
}
