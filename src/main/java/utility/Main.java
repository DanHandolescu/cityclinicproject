package utility;

import models.Clients;
import org.hibernate.SessionFactory;
import repository.ClientsRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        Path filePath = Paths.get("C:\\Users\\Dan\\Desktop\\Practical Project\\CityClinicProject\\src\\main\\resources\\clients.csv");
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        ClientsRepository clientsRepository = new ClientsRepository(sessionFactory.createEntityManager());

        try {
            List<String> lines = Files.readAllLines(filePath);
            for (String line : lines) {
                Clients clients = ClientsParser.fromCSV(line);
                clientsRepository.saveClients(clients);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
