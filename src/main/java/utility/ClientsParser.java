package utility;

import models.Clients;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ClientsParser {
    public ClientsParser() {
    }

    public static Clients fromCSV(String line) {
        String[] data = line.split(",");
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        new SimpleDateFormat("dd-MM-YYYY");
        return new Clients(data[0], data[1], data[2], LocalDate.parse(data[3], formatter1));
    }
    public static String toCSV(Clients client){
        StringBuilder sb= new StringBuilder();
        sb.append(client.getName());
        sb.append(",");
        sb.append(client.getAddress());
        sb.append(",");
        sb.append(client.getPhoneNr());
        sb.append(",");
        sb.append(client.getBirthday());
        sb.append("\n");
        return sb.toString();
    }
}
